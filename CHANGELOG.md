# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.2.0](https://bitbucket.org/sasahou/testmsg/compare/v3.1.2-beta.0...v3.2.0) (2021-04-15)


### Bug Fixes

* test ([32f3e08](https://bitbucket.org/sasahou/testmsg/commit/32f3e089263cc8b57f9ab44b7d16d0c0623ad8c9))

### [3.1.2-beta.0](https://bitbucket.org/sasahou/testmsg/compare/v3.1.1-beat.0...v3.1.2-beta.0) (2021-04-15)


### Bug Fixes

* test ([5e1485a](https://bitbucket.org/sasahou/testmsg/commit/5e1485a8004ff447e43497ec1733e65d4e9cc612))

### [3.1.1-beat.0](https://bitbucket.org/sasahou/testmsg/compare/v3.1.0...v3.1.1-beat.0) (2021-04-15)


### Bug Fixes

* test ([1d8d1ba](https://bitbucket.org/sasahou/testmsg/commit/1d8d1ba6c38eaf2fab3c77be877bc251b711b731))

## [3.1.0](https://bitbucket.org/sasahou/testmsg/compare/v3.0.0...v3.1.0) (2021-04-15)


### Bug Fixes

* test13 ([cce5662](https://bitbucket.org/sasahou/testmsg/commit/cce56628d18de08c05c7f9be2b5c646aec90ec8e))

## [3.0.0](https://bitbucket.org/sasahou/testmsg/compare/v2.0.1...v3.0.0) (2021-04-15)


### Bug Fixes

* test12 ([2898fa3](https://bitbucket.org/sasahou/testmsg/commit/2898fa3ea4ffbd27ae5a27f9b35688ddcb6e2fc5))

### [2.0.1](https://bitbucket.org/sasahou/testmsg/compare/v1.1.1...v2.0.1) (2021-04-15)


### Bug Fixes

* test11 ([ad29918](https://bitbucket.org/sasahou/testmsg/commit/ad29918fbe9d073d169a0787bcfa39f7f419c3ec))

### [1.1.1](https://bitbucket.org/sasahou/testmsg/compare/v1.1.0...v1.1.1) (2021-04-15)


### Bug Fixes

* test10 ([4480cc5](https://bitbucket.org/sasahou/testmsg/commit/4480cc51d35094438d7a8c5729abd74a0520e060))

## [1.1.0](https://bitbucket.org/sasahou/testmsg/compare/v1.0.3...v1.1.0) (2021-04-15)


### Bug Fixes

* test9 ([7bbda9b](https://bitbucket.org/sasahou/testmsg/commit/7bbda9b21f6f59642214745fbaccc045899ae3d4))

### [1.0.3](https://bitbucket.org/sasahou/testmsg/compare/v1.0.3-beta.2...v1.0.3) (2021-04-15)


### Bug Fixes

* test8 ([3c1229a](https://bitbucket.org/sasahou/testmsg/commit/3c1229a49760f5ca946db06601b8f95a00fe69a1))

### [1.0.3-beta.2](https://bitbucket.org/sasahou/testmsg/compare/v1.0.3-beta.1...v1.0.3-beta.2) (2021-04-15)


### Bug Fixes

* test7 ([a955d2c](https://bitbucket.org/sasahou/testmsg/commit/a955d2c089b10706a7b27dec87b2c12eb427dd52))

### [1.0.3-beta.1](https://bitbucket.org/sasahou/testmsg/compare/v1.0.2...v1.0.3-beta.1) (2021-04-15)


### Bug Fixes

* test6 ([b6298ca](https://bitbucket.org/sasahou/testmsg/commit/b6298ca1b5670b20e7086bcefb3af505bdd411a6))

### [1.0.2](https://bitbucket.org/sasahou/testmsg/compare/v1.0.1...v1.0.2) (2021-04-15)


### Bug Fixes

* test4 ([057f402](https://bitbucket.org/sasahou/testmsg/commit/057f40251692ed56ca5415fc0b46c87f3fb08c46))
* test5 ([b474087](https://bitbucket.org/sasahou/testmsg/commit/b4740870c3929e2f647e394ad34842a7811a7803))

### [1.0.1](https://bitbucket.org/sasahou/testmsg/compare/v1.0.1-beta.3...v1.0.1) (2021-04-15)


### Bug Fixes

* test3 ([68179c1](https://bitbucket.org/sasahou/testmsg/commit/68179c10edf05b20f264ce5491464621d7c50a03))
